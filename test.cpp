#include <iostream>
#include <sstream>
#include <fstream>
#include "smtparser/lexer.hpp"
#include "smtparser/parser.hpp"
#include "smtparser/ast.hpp"

int main (int argc, char**argv) {
  std::fstream str;
  str.open(argv[1]);
  SMTParser::Parser parser;
  SMTParser::ErrorHandler error;
   parser.Parse (str,error);
   for (auto res : parser.getAssert ())
     std::cerr << *res << std::endl;
}
