%{
#include <ostream>
#include <memory>
#include <istream>
#include "smtparser/tokens.hpp"
#include "smtparser/lexer.hpp"
  using namespace std;
  using namespace SMTParser;
#define YY_USER_ACTION							\
  column += yyleng;
%}

%option noyywrap
%option yyclass="SMTLexer"
   
ws      [ \t]+
alpha   [A-Za-z]
numeral 0|([1-9][0-9]*)
decimal numeral[.]([0-9]+)
binary #b[1]+
string \"([^\"])*\"
symbol [a-zA-Z~!@$%^&*_+=<>.?/-][0-9a-zA-Z~!@$%^&*_+=<>.?/-]*
keyword :[0-9a-zA-Z~!@$%^&*_+=<>.?/-]+
%%

{ws}    /* skip blanks and tabs */


"(" return toInt(Tokens::LPARAN);
")" return toInt(Tokens::RPARAN);

\n        { mylineno++;column = 0;}

{string} {return toInt(Tokens::STRING);}
{symbol}   { return toInt(ReservedChecker::isReserved (yytext) ? Tokens::RESERVED : Tokens::SYMBOL);}
{numeral} { return toInt(Tokens::NUMERAL);}
{decimal} { return toInt(Tokens::DECIMAL);}
{binary} { return toInt(Tokens::BINARY);}
{keyword} {return toInt(Tokens::KEYWORD);}

%%
std::unique_ptr<SMTParser::SMTLexer>  SMTParser::makeFlexer (std::istream& i) {
  auto a= std::make_unique<SMTParser::SMTLexer> ();
  a->yyrestart (i);
  return a; 
}
