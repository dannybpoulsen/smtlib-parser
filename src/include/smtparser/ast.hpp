#ifndef _AST__
#define _AST__

#include <stdexcept>
#include <memory>
#include <ostream>
#include <vector>


enum class Sort {
				 Integer,
				 String,
				 Real,
				 Bool,
				 Command
};

class NumericLiteral;
class StringLiteral;
class Identifier;
class Conjunction;
class Disjunction;
class Not;
class LEQ;
class LT;
class GEQ;
class GT;
class EQ;
class NEQ;
class Plus;
class FunctionApplication;
class Multiplication;
class StrConcat;
class StrLen;
class DeclareFun;
class SetLogic;
class Assert;
class NegLiteral;


class ASTVisitor {
public:
  virtual void caseNumericLiteral ( NumericLiteral& ) {}
  virtual void caseStringLiteral ( StringLiteral& ) {}
  virtual void caseIdentifier ( Identifier& ) {}
  virtual void caseConjunction ( Conjunction& ) = 0;
  virtual void caseDisjunction ( Disjunction& ) = 0;
  virtual void caseLEQ ( LEQ& ) = 0;
  virtual void caseLT ( LT& ) = 0;
  virtual void caseGEQ ( GEQ& ) = 0;
  virtual void caseGT ( GT& ) = 0;
  virtual void caseEQ ( EQ& ) = 0;
  virtual void caseNEQ ( NEQ& ) = 0;
  virtual void casePlus ( Plus& ) = 0;
  virtual void caseMultiplication ( Multiplication& ) = 0;
  virtual void caseStrConcat ( StrConcat& ) = 0;
  virtual void caseStrLen ( StrLen& ) = 0;
  virtual void caseDeclareFun ( DeclareFun& ) = 0;
  virtual void caseSetLogic ( SetLogic& ) = 0;
  virtual void caseAssert ( Assert& ) = 0;
  virtual void caseNot ( Not&) = 0;
  virtual void caseNegLiteral ( NegLiteral&) = 0;
  virtual void caseFunctionApplication ( FunctionApplication&) = 0;
};

class ASTNode  {
public:
  virtual ~ASTNode() {}
  virtual std::ostream& toString (std::ostream&) const = 0;
  virtual void accept(ASTVisitor& v) = 0;
  virtual Sort getSort () const = 0;
  virtual void setSort (Sort) {throw std::runtime_error ("Can't change sort");}
  auto hash () const {
    std::stringstream str;
    this->toString (str);
    return std::hash<std::string>{}  (str.str());
  }

};

using ASTNode_ptr = std::shared_ptr<ASTNode> ;


inline std::ostream& operator << (std::ostream& os, const ASTNode& ast) {
  return ast.toString (os);
}

template<typename T>
class BaseLiteral {
public:
  BaseLiteral (const T& t) : val(t) {}
  const T& getVal () const {return val;}
private:
  T val;
};

class Symbol : public BaseLiteral<std::string>,
	       public std::enable_shared_from_this<Symbol>
{
public:
  Symbol (const std::string& t) : BaseLiteral<std::string> (t) {}
  virtual Sort getSort () const {return sort;}
  virtual void setSort (Sort s) {sort = s;}

private:
  Sort sort;
};

class NumericLiteral :public BaseLiteral<int64_t>,
					  public ASTNode,
					  public std::enable_shared_from_this<NumericLiteral>
{
public:
  NumericLiteral (int64_t t) : BaseLiteral<int64_t> (t) {}
  virtual Sort getSort () const {return Sort::Integer;}
  virtual void accept(ASTVisitor& v) {v.caseNumericLiteral (*this);}
  virtual std::ostream& toString (std::ostream& os) const { return os << " " <<getVal() << " ";}

};
  
class DecimalLiteral :public BaseLiteral<double>,
		      public ASTNode,
		      public std::enable_shared_from_this<DecimalLiteral>
{
public:
  DecimalLiteral (double t) : BaseLiteral<double> (t) {}
  virtual Sort getSort () const {return Sort::Real;}
  virtual void accept(ASTVisitor& v) {}
  virtual std::ostream& toString (std::ostream& os) const { return os <<  " " << getVal() <<" ";}
};

class StringLiteral :public BaseLiteral<std::string>,
					 public ASTNode,
					 public std::enable_shared_from_this<StringLiteral>
{
public:
  StringLiteral (const std::string& t) : BaseLiteral<std::string> (t) {}
  virtual Sort getSort () const {return Sort::String;}
  virtual void accept(ASTVisitor& v) {v.caseStringLiteral (*this);}
  virtual std::ostream& toString (std::ostream& os) const { return os << " " << getVal() << " ";}
};

class Identifier : public ASTNode,
				   public std::enable_shared_from_this<Identifier>
{
public:
  Identifier (Symbol& s) : symbol(s.shared_from_this()) {}
  std::ostream& toString (std::ostream& os) const { return os << symbol->getVal ();}
  Sort getSort () const {return symbol->getSort ();}
  virtual void accept(ASTVisitor& v) {v.caseIdentifier (*this);}
  auto& getSymbol () const {return symbol;}
private:
  std::shared_ptr<Symbol> symbol;
};


class NegLiteral : public ASTNode,
				   public std::enable_shared_from_this<NegLiteral>
{
public:
  NegLiteral (Identifier& s) : ast(s.shared_from_this()) {}
  std::ostream& toString (std::ostream& os) const { return os << "(! " << *ast << ")";}
  Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseNegLiteral (*this);}
  auto inner () {return ast;}
private:
  std::shared_ptr<Identifier> ast;
};

class DummyApplication  : public ASTNode
{
public:
  DummyApplication (std::vector<std::shared_ptr<ASTNode> >& s) : subs(s) {}
  
  auto begin () const {return subs.begin();}
  auto  end () const {return subs.end();}
  auto getExpr (size_t i) const  {return subs[i];}
  virtual std::ostream& toString (std::ostream& os) const { return os << "FUNCTION";}

protected:
  auto getSize () {return subs.size();}
  std::vector<std::shared_ptr<ASTNode> > subs;
  Sort sort;
};

class FunctionApplication  : public DummyApplication,
							 public std::enable_shared_from_this<FunctionApplication>
{
public:
  FunctionApplication (Symbol& id,  std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s),
										  identifier(id.shared_from_this()) {}
  virtual Sort getSort () const {return identifier->getSort ();}
  
  auto getIdentifier () {return identifier;}
  virtual void accept(ASTVisitor& v) {v.caseFunctionApplication (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(";
    os << identifier->getVal ();
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
private:
  std::shared_ptr<Symbol> identifier;
};


class Conjunction : public DummyApplication,
					public std::enable_shared_from_this<Conjunction> 
{
public:
  Conjunction (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseConjunction (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(and ";
    
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
};

class Not : public DummyApplication,
					public std::enable_shared_from_this<Not> 
{
public:
  Not (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseNot (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(not ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
};

class Disjunction : public DummyApplication,
					public std::enable_shared_from_this<Disjunction> 
{
public:
  Disjunction (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseDisjunction (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(or ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
};



class LEQ : public DummyApplication,
					public std::enable_shared_from_this<LEQ> 
{
public:
  LEQ (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseLEQ (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(<= ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
  
};

class LT : public DummyApplication,
					public std::enable_shared_from_this<LT> 
{
public:
  LT (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseLT (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(< ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
  
};

class GEQ : public DummyApplication,
					public std::enable_shared_from_this<GEQ> 
{
public:
  GEQ (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseGEQ (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(>= ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
  
};

class GT : public DummyApplication,
					public std::enable_shared_from_this<GT> 
{
public:
  GT (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Bool;}
    virtual void accept(ASTVisitor& v) {v.caseGT (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(> ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
  
};

class EQ : public DummyApplication,
					public std::enable_shared_from_this<EQ> 
{
public:
  EQ (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseEQ (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(= " ;
    for (auto& ast : *this)
      ast->toString (os);
    return os << " )";
  }
  
};

class NEQ : public ASTNode,
	    public std::enable_shared_from_this<NEQ> 
{
public:
  NEQ (EQ& s) :  eq(s.shared_from_this ()) {
    
  }
  virtual Sort getSort () const {return Sort::Bool;}
  virtual void accept(ASTVisitor& v) {v.caseNEQ (*this);}
  auto begin () const {return eq->begin();}
  auto  end () const {return eq->end();}
  auto getExpr (size_t i) const  {return eq->getExpr(i);}

  auto getInnerEQ () { return eq;}
  
  std::ostream& toString (std::ostream& os) const {
    os << "(!= ";
    for (auto& ast : *eq)
      ast->toString (os);
    return os << " )";
  }


private:
  std::shared_ptr<EQ> eq;
};

class Plus : public DummyApplication,
			 public std::enable_shared_from_this<Plus       > 
{
public:
  Plus (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Integer;}
    virtual void accept(ASTVisitor& v) {v.casePlus (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(+ ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << " )";
  }
  
};

class Multiplication : public DummyApplication,
					   public std::enable_shared_from_this<Multiplication> 
{
public:
  Multiplication (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Integer;}
  virtual void accept(ASTVisitor& v) {v.caseMultiplication (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(* ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << ")";
  }
  
};

class StrConcat : public DummyApplication,
					public std::enable_shared_from_this<StrConcat> 
{
public:
  StrConcat (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::String;}
    virtual void accept(ASTVisitor& v) {v.caseStrConcat (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(str.++ ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << " )";
  }
  
};


class StrLen : public DummyApplication,
			   public std::enable_shared_from_this<StrLen> 
{
public:
  StrLen (std::vector<std::shared_ptr<ASTNode> >&& s) : DummyApplication (s) {}
  virtual Sort getSort () const {return Sort::Integer;}
  virtual void accept(ASTVisitor& v) {v.caseStrLen (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(str.len ";
    for (auto& ast : *this)
      ast->toString (os);
    return os << " )";
  }
  

};



class Command : public ASTNode {
public:
  virtual Sort getSort () const { return Sort::Command;}
  
};

class DeclareFun : public Command {
public:
  DeclareFun (  std::shared_ptr<Identifier>& id) : ides(id) {}
  virtual void accept(ASTVisitor& v) {v.caseDeclareFun (*this);}
  std::ostream& toString (std::ostream& os) const {
    os << "(declare-fun";
    ides->toString (os);
    return os << ")";
  }
  
private:
  std::shared_ptr<Identifier> ides;
};

class SetLogic  : public Command,
				  public std::enable_shared_from_this<SetLogic>
{
public:
  SetLogic (Symbol& s) : symb(s.shared_from_this()) {}
  std::ostream& toString (std::ostream& os) const { return os << "SetLogic";}
  virtual void accept(ASTVisitor& v) {v.caseSetLogic (*this);}
private:
  std::shared_ptr<Symbol> symb;;
};

class Assert  : public Command,
		public std::enable_shared_from_this<Assert>
{
public:
  Assert(std::shared_ptr<ASTNode>& s) : symb(s) {}
  virtual void accept(ASTVisitor& v) {v.caseAssert (*this);}
  auto& getExpr () const  { return symb;}
  std::ostream& toString (std::ostream& os) const {
    os << "(assert";
    
    symb->toString (os);
    return os << ")";
  }
  
private:
  std::shared_ptr<ASTNode> symb;;
};

class BaseVisitor : public ASTVisitor {
public:
  virtual void caseConjunction ( Conjunction& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }

  virtual void caseDisjunction ( Disjunction& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }

  virtual void caseLEQ ( LEQ& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }
  virtual void caseLT ( LT& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }

    virtual void caseNEQ ( NEQ& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }

  
  virtual void caseGEQ ( GEQ& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }
  
  virtual void caseGT ( GT& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }
  
  virtual void caseEQ ( EQ& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }
  
  virtual void casePlus ( Plus& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }
  
  virtual void caseMultiplication ( Multiplication& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }
  virtual void caseStrConcat ( StrConcat& c)
 {
	for (auto& cc : c)
	  cc->accept (*this);
  }
  
  virtual void caseStrLen ( StrLen& c)
  {
	for (auto& cc : c)
	  cc->accept (*this);
  }
  
  virtual void caseDeclareFun ( DeclareFun&)
  {
  }

  virtual void caseSetLogic ( SetLogic&)
  {

  }

  virtual void caseAssert ( Assert&)
  {
	
  }

  virtual void caseNot ( Not&)
  {
	
  }

  virtual void caseNegLiteral ( NegLiteral&)
  {
	
  }

  


};



class NNFConverter: public BaseVisitor {
public:
  auto Convert ( ASTNode_ptr& ptr) {
	ptr->accept(*this);
	return cur;
  }
  
  template<class T>
  auto visitAndRetVec (T& t) {
    std::vector<ASTNode_ptr> vec;
    for (auto& cc : t) {
      cc->accept (*this);
      assert(cur);
      vec.push_back (cur);
    }
    return vec;
  }

  virtual void caseNumericLiteral ( NumericLiteral& c) {
    cur= c.shared_from_this ();
  }
  virtual void caseStringLiteral ( StringLiteral& c) {
    cur= c.shared_from_this ();
  }
  
  virtual void caseIdentifier ( Identifier& c) {
    if (underNeg && c.getSort () ==Sort::Bool)
      cur = std::make_shared<NegLiteral> (c);
    else 
      cur= c.shared_from_this ();
  }

  virtual void casePlus ( Plus& c ) {
    cur= c.shared_from_this ();
  }
  
  virtual void caseMultiplication ( Multiplication& c) {
    cur= c.shared_from_this ();
  }
  
  virtual void caseStrConcat ( StrConcat& c) {
    cur= c.shared_from_this ();
  }

  virtual void caseFunctionApplication ( FunctionApplication& c) {
    cur= c.shared_from_this ();
  }

  
  virtual void caseStrLen ( StrLen& c) {
    cur= c.shared_from_this ();
  }
  

  
  virtual void caseConjunction ( Conjunction& c)
  {
    
    if(!underNeg)
      cur = std::make_shared<Conjunction> (visitAndRetVec (c));
    else {
      cur = std::make_shared<Disjunction> (visitAndRetVec (c));
    }
    
  }

  virtual void caseDisjunction ( Disjunction& c)
  {
    if(!underNeg)
      cur = std::make_shared<Disjunction> (visitAndRetVec (c));
    else {
      cur = std::make_shared<Conjunction> (visitAndRetVec (c));
    }
	
  }

  virtual void caseLEQ ( LEQ& c)
  {
    if(!underNeg)
      cur = std::static_pointer_cast<ASTNode> (c.shared_from_this ());
    else {
      cur = std::make_shared<GT> (visitAndRetVec (c));
    }
  }
  virtual void caseLT ( LT& c)
  {
    if(!underNeg)
      cur = std::static_pointer_cast<ASTNode> (c.shared_from_this ());
    else {
      cur = std::make_shared<GEQ> (visitAndRetVec (c));
    }
  }
  virtual void caseGEQ ( GEQ& c)
  {
    if(!underNeg)
      cur = std::static_pointer_cast<ASTNode> (c.shared_from_this ());
    else {
      cur = std::make_shared<LT> (visitAndRetVec (c));
    }
  }
  virtual void caseGT ( GT& c)
  {
    if(!underNeg)
      cur = std::static_pointer_cast<ASTNode> (c.shared_from_this ());
    else {
      cur = std::make_shared<LEQ> (visitAndRetVec (c));
    }
  }
  
  virtual void caseEQ ( EQ& c)
  {
    if(!underNeg)
      cur = std::static_pointer_cast<ASTNode> (c.shared_from_this ());
    else {
      cur = std::make_shared<NEQ> (c);
    }
  }

  virtual void caseNEQ ( NEQ& c)
  {
    if(!underNeg)
      cur = std::static_pointer_cast<ASTNode> (c.shared_from_this ());
    else {
      cur = std::make_shared<EQ> (visitAndRetVec (c));
    }
  }

  virtual void caseNot ( Not& m)
  {
    underNeg = !underNeg;
    auto vec = visitAndRetVec (m);
    assert(vec.size () == 1);
    cur = vec.at(0);
    underNeg = !underNeg;
  }

  virtual void caseNegLiteral ( NegLiteral&)
  {
    
  }
  
private:
  ASTNode_ptr cur;
  bool underNeg = false;
};



#endif
