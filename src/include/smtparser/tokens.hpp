#ifndef _TOKENS__
#define _TOKENS__

#include <memory>
#include <ostream>
namespace SMTParser {
#define TOKENS									\
  X(DUMMY)										\
  X(NUMERAL)									\
  X(DECIMAL)									\
  X(BINARY)                                     \
  X(HEX)										\
  X(STRING)										\
  X(SYMBOL)										\
  X(KEYWORD)									\
  X(RESERVED)									\
  X(LPARAN)										\
  X(RPARAN)										\



enum class Tokens {
#define X(name)	   name,
				   TOKENS
#undef X 
};



enum class Reserved {
			   ASSERT,
			   CHECKSAT,
			   DECLAREFUN,
			   DECLARESORT,
			   DEFINEFUN,
			   DEFINESORT,
			   EXIT,
			   GETASSERTIONS,
			   GETASSIGNMENT,
			   GETINFO,
			   GETOPTION,
			   GETPROOF,
			   GETUNSATCORE,
			   GETVALUE,
			   POP,
			   PUSH,
			   SETINFO,
			   SETLOGIC,
			   SETOPTION,
			   PAR,
			   NUMERAL,
			   DECIMAL,
			   STRING,
			   BOOL,
			   UNDERSCORE,
			   BANG,
			   AS,
			   LET,
			   FORALL,
			   EXISTS,
			   LEQ,
			   GEQ,
			   EQ,
			   LT,
			   GT,
			   NEGATION,
			   PLUS,
			   MULT,
			   AND,
			   OR,
			   NOT,
			   CONCAT,
			   LENGTH,
			   ITE
};
  

enum ReservedType {
				   SCRIPT,
				   NORMAL,
				   STDFUNCNAME,
				   STRFUNCNAME
};


struct ReservedInfo {
  const char* name;
  ReservedType type;
  Reserved keyword;
};

struct LexObject {
  LexObject (std::string s,Tokens tok, size_t lineno, size_t colStart) : text(s),token(tok),lineno(lineno),colStart(colStart) {} 
  LexObject () {}
  std::string text;
  Tokens token;
  size_t lineno;
  size_t colStart;
};

inline std::ostream& operator<< (std::ostream& os, const Tokens t) {
  switch(t) {
#define X(name)									\
	case Tokens::name: return os << #name;		
	TOKENS
#undef X
	
	
	  }
  return os;
}

class ReservedChecker {
public:
  static bool isReserved (std::string name);
  static Reserved getReserved (std::string name);
};


inline std::ostream& operator<< (std::ostream& os, const LexObject& obj) {
  return os << obj.text  << " " << obj.token << " " << obj.lineno <<"."<< obj.colStart;
}

}

#endif

