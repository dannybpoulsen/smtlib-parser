#ifndef _PARSER__
#define _PARSER__
#include <istream>
#include <cassert>
#include <unordered_map>
#include <vector>
#include <initializer_list>

#include "lexer.hpp"
#include "ast.hpp"
namespace SMTParser {
class ErrorHandler {
public:
  virtual void error (const std::string& err) {
	std::cerr << err << std::endl;
  };
};

class Parser {
public:
  void Parse (std::istream& is,ErrorHandler& h) {
	handler = &h;
	lexer = makeFlexer (is);
	lex ();
	ASTNode_ptr ptr = parseCommand ();
	while (ptr) {
	  ptr = parseCommand (); 
	}
	
  }

  auto& getAssert () {
	return asserts;
  }

  auto& getVars () {
	return vars;
  }
  
private:
  ASTNode_ptr parseSimp (){
	auto id = parseIdentifier ();
	if (id)
	  return id;
	else
	  return parseLiteral ();
  }
  
  ASTNode_ptr parseLiteral () {
	if (tryaccept (Tokens::NUMERAL)) {
	  return std::make_shared<NumericLiteral> (std::stoi (object.text));
	}

	else if (tryaccept (Tokens::STRING)) {
	  std::string str = object.text;
	  str.pop_back();
	  str.erase(str.begin());
	  return std::make_shared<StringLiteral> (str);
	  
	}
	
	return nullptr;
  }

  std::shared_ptr<Symbol> parseSymbol () {
	if (tryaccept (Tokens::SYMBOL) ) {
	  return std::make_shared<Symbol> (object.text);
	}

	return nullptr;
  }

  std::shared_ptr<Identifier> parseIdentifier () {
	auto symb = parseSymbol ();
	if (symb)
	  return identifiers.at(static_cast<Symbol*>(symb.get())->getVal ());
	else
	  return nullptr;
  }

  ASTNode_ptr parseQualifiedIdentifier () {
	return parseIdentifier ();
  }

  Sort parseSort () {
    auto symb = parseSymbol ();
    if (symb->getVal () == "String") {
      return Sort::String;
    }
    
    if (symb->getVal () == "Integer") {
      return Sort::Integer;
    }
    
    if (symb->getVal () == "Bool") {
      return Sort::Bool;
    }
    
    handler->error ("Not a sort");
  }

  std::vector<ASTNode_ptr> parseSub () {
	std::vector<ASTNode_ptr> vec;
	ASTNode_ptr ptr = parseExpression ();
	while (ptr) {
	  vec.push_back (ptr);
	  ptr = parseExpression ();
	}
	return vec;
  }
  
  ASTNode_ptr parseExpression () {
	if (tryaccept (Tokens::LPARAN)) {
	  
	  if (tryaccept (Reserved::PLUS)) {
		std::vector<ASTNode_ptr> vec = parseSub ();
		for (auto& t : vec) {
		  if (t->getSort () != Sort::Integer)
			handler->error ("Plus operands mush have type Integer");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<Plus> (std::move(vec));
	  }


	  else if (tryaccept (Reserved::AND)) {
	    std::vector<ASTNode_ptr> vec = parseSub ();
	    for (auto& t : vec) {
	      if (t->getSort () != Sort::Bool)
		handler->error ("And operands mush have type Bool");
	    }
	    accept(Tokens::RPARAN);
	    return std::make_shared<Conjunction> (std::move(vec));
	  }

	  else if (tryaccept (Reserved::NOT)) {
		std::vector<ASTNode_ptr> vec = parseSub ();
		for (auto& t : vec) {
		  if (t->getSort () != Sort::Bool)
		    handler->error ("Not operands mush have type Bool");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<Not> (std::move(vec));
	  }

	  else if (tryaccept (Reserved::OR)) {
		std::vector<ASTNode_ptr> vec = parseSub ();
		for (auto& t : vec) {
		  if (t->getSort () != Sort::Bool)
			handler->error ("Or operands mush have type Bool");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<Disjunction> (std::move(vec));
	  }
	  
	  else if (tryaccept (Reserved::MULT)) {
		std::vector<ASTNode_ptr> vec = parseSub ();
		for (auto& t : vec) {
		  if (t->getSort () != Sort::Integer)
		    handler->error ("Mult operands mush have type Integer");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<Multiplication> (std::move(vec));
			
	  }

	  else if (tryaccept (Reserved::NEGATION)) {
		accept (Tokens::NUMERAL);
		auto i = std::stoi (object.text);
		accept(Tokens::RPARAN);
		return std::make_shared<NumericLiteral> (-i);
		
	  }

	  else if (tryaccept (Reserved::LEQ)) {
		std::vector<ASTNode_ptr> vec = parseSub ();

		if (vec.size() != 2) {
		  handler->error ("LEQ Can only have two operands");
		}
		for (auto& t : vec) {
		  if (t->getSort () != Sort::Integer)
			handler->error ("LEQ operands mush have type Integer");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<LEQ> (std::move(vec));
	  }

	  else if (tryaccept (Reserved::LT)) {
		std::vector<ASTNode_ptr> vec = parseSub ();

		if (vec.size() != 2) {
		  handler->error ("LT can only have two operands");
		}
		for (auto& t : vec) {
		  if (t->getSort () != Sort::Integer)
			handler->error ("LT operands mush have type Integer");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<LT> (std::move(vec));
		
	  }

	  else if (tryaccept (Reserved::EQ)) {
		std::vector<ASTNode_ptr> vec = parseSub ();

		if (vec.size() != 2) {
		  handler->error ("Eq can only have two operands");
		}
		bool first = true;
		Sort sort = Sort::Integer;
		for (auto& t : vec) {
		  if (first)
			sort = t->getSort ();
		  else if (t->getSort () != sort)
			handler->error (" EQ Operands mush have same type");
		  first = false;
		}
		accept(Tokens::RPARAN);
		if (sort == Sort::Bool) {
		  auto conjvec = std::make_shared<Conjunction> (std::initializer_list<ASTNode_ptr> ({vec[0],vec[1]}));
		  auto negl = std::make_shared<Not> ( std::initializer_list<ASTNode_ptr> ({vec[0]}));
		  auto negr = std::make_shared<Not> ( std::initializer_list<ASTNode_ptr> ({vec[1]}));
		  auto conjnvec = std::make_shared<Conjunction> (std::initializer_list<ASTNode_ptr> ({negl,negr}));
		  return std::make_shared<Disjunction> (std::initializer_list<ASTNode_ptr> ({conjvec,conjnvec}));
		  
		}
		else if (sort == Sort::Integer) {
		  std::vector<ASTNode_ptr> vec2 = vec;
		  auto leq = std::make_shared<LEQ> (std::move(vec));
		  auto geq = std::make_shared<GEQ> (std::move(vec));
		  return std::make_shared<Conjunction> (std::initializer_list<ASTNode_ptr> ({leq,geq}));
		}
		return std::make_shared<EQ> (std::move(vec));
		
	  }

	  else if (tryaccept (Reserved::GEQ)) {
		std::vector<ASTNode_ptr> vec = parseSub ();

		if (vec.size() != 2) {
		  handler->error ("GEQ Can only have two operands");
		}
		for (auto& t : vec) {
		  if (t->getSort () != Sort::Integer)
			handler->error ("GEQ operands mush have type Integer");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<GEQ> (std::move(vec));
	  }

	  else if (tryaccept (Reserved::GT)) {
		std::vector<ASTNode_ptr> vec = parseSub ();

		if (vec.size() != 2) {
		  handler->error ("GT Can only have two operands");
		}
		for (auto& t : vec) {
		  if (t->getSort () != Sort::Integer)
			handler->error ("GT operands mush have type Integer");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<GT> (std::move(vec));
	  }

	  else if (tryaccept (Reserved::CONCAT)) {
		std::vector<ASTNode_ptr> vec = parseSub ();
		for (auto& t : vec) {
		  if (t->getSort () != Sort::String)
			handler->error ("COncat operands mush have type String");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<StrConcat> (std::move(vec));
	  }

	  else if (tryaccept (Reserved::LENGTH)) {
		std::vector<ASTNode_ptr> vec = parseSub ();
		for (auto& t : vec) {
		  if (t->getSort () != Sort::String)
			handler->error ("str.len operands mush have type String");
		}
		if (vec.size()!= 1) {
		  handler->error ("str.len can only have one operand");
		}
		accept(Tokens::RPARAN);
		return std::make_shared<StrLen> (std::move(vec));
	  }

	  
	  else if (tryaccept (Reserved::ITE)) {
		std::vector<ASTNode_ptr> vec = parseSub ();
		if (vec.size()!= 3) {
		  handler->error ("ite must have 3 operands");
		}
		if (vec[0]->getSort () != Sort::Bool ||  (vec[1]->getSort () != vec[2]->getSort () )) {
		  handler->error ("ite(0) must have type Bool and ite(1) and ite(2) must have same type");
		}
		accept(Tokens::RPARAN);
		std::shared_ptr<Identifier> ite = makeAnonIdentifier (vec[1]->getSort ());
		auto tt = std::make_shared<EQ> (std::initializer_list<ASTNode_ptr> ({vec[1],ite}));
		auto ff = std::make_shared<EQ> (std::initializer_list<ASTNode_ptr> ({vec[2],ite}));
		auto conj1 = std::make_shared<Conjunction> (std::initializer_list<ASTNode_ptr> ({vec[0],tt}));
		std::shared_ptr<ASTNode> nn = std::make_shared<Not> (std::initializer_list<ASTNode_ptr> ({vec[0]}));
		auto nnn = converter.Convert (nn);
		auto conj2 = std::make_shared<Conjunction> (std::initializer_list<ASTNode_ptr> ({nnn,ff}));
		ASTNode_ptr dicj = std::make_shared<Disjunction> (std::initializer_list<ASTNode_ptr> ({conj1,conj2}));
		auto asse = std::make_shared<Assert> (dicj);
		asserts.push_back(asse);
		
		return ite;
	  }

	  
	  else {
	  
		auto ids = parseSymbol ();
		assert(ids);
		std::vector<ASTNode_ptr> vec = parseSub ();
		
		
		accept (Tokens::RPARAN);
		return std::make_shared<FunctionApplication> (*ids,std::move(vec));
	  }
	}
	
	
	return parseSimp ();
  }

  
  
  ASTNode_ptr parseCommand () {
	ASTNode_ptr ast = nullptr;
	if (tryaccept (Tokens::LPARAN)) {
	  if (tryaccept (Reserved::SETLOGIC) ) {
		auto symb = parseSymbol ();
		assert(symb);
		ast =  std::make_shared<SetLogic> (*symb);
	  }
	  
	  else if (tryaccept (Reserved::ASSERT )) {
		auto theassert = parseExpression ();
		assert(theassert->getSort () == Sort::Bool);
		auto innnf = converter.Convert(theassert);
		ast = std::make_shared<Assert> (innnf);
		asserts.push_back (ast);
	  }
	  
	  else if (tryaccept (Reserved::DECLAREFUN) ) {
		auto funname = parseSymbol ();
		accept(Tokens::LPARAN);
		accept(Tokens::RPARAN);
		auto sort = parseSort ();
		funname->setSort (sort);
		
		auto ides = std::make_shared<Identifier> (*funname);
		identifiers.insert (std::make_pair (funname->getVal(),ides));
		vars.push_back(ides);
		ast =  std::make_shared<DeclareFun> (ides);
		
	  }
	  else if (tryaccept (Reserved::DEFINEFUN) ||
		   tryaccept (Reserved::DECLARESORT) ||
		   tryaccept (Reserved::DEFINESORT) ||
		   tryaccept (Reserved::GETASSERTIONS) ||
		   tryaccept (Reserved::GETPROOF) ||
		   tryaccept (Reserved::GETUNSATCORE) ||
		   tryaccept (Reserved::GETVALUE) ||
		   tryaccept (Reserved::GETASSIGNMENT) ||
		   tryaccept (Reserved::PUSH) ||
		   tryaccept (Reserved::POP) ||
		   tryaccept (Reserved::GETOPTION) ||
		   tryaccept (Reserved::SETOPTION) ||
		   tryaccept (Reserved::GETINFO) ||
		   tryaccept (Reserved::SETINFO) ||
		   tryaccept (Reserved::EXIT) 
		   )   {
	    handler->error ("Error Unsupported Command");
	    return nullptr;
	  }

	  else if (tryaccept(Reserved::CHECKSAT)) {
	    //do nothing
	    //We always check satisfiability of the input
	  }
	  
	  accept (Tokens::RPARAN);
	}
	
	return ast;
  }

  
  void unexpected () {
    std::stringstream str;
    str<< "Unexpected: "<< objectBuf; 
    handler-> error (str.str());
  }
  
  bool accept (Tokens tok) {
	if (tok == objectBuf.token) {
	  object = objectBuf;
	  lex ();
	  return true;
	}
	unexpected ();
	return false;
  }

  bool tryaccept (Tokens tok) {
	if (tok == objectBuf.token) {
	  object = objectBuf;
	  lex ();
	  return true;
	}
	return false;
  }

  bool tryaccept (Reserved r) {
	if (Tokens::RESERVED == objectBuf.token && r == ReservedChecker::getReserved (objectBuf.text)) {
	  object = objectBuf;
	  lex ();
	  return true;
	}
	return false;
  }
  
  bool accept (Reserved r) {
	if (Tokens::RESERVED == objectBuf.token && r == ReservedChecker::getReserved (objectBuf.text)) {
	  object = objectBuf;
	  lex ();
	  return true;
	}
	unexpected ();
	return false;
  }

  void lex () {
	if (lexer->yylex()) {
	  objectBuf = lexer->getLex ();
	}
	else 
	  objectBuf.token = Tokens::DUMMY;
  }

  std::shared_ptr<Identifier> makeAnonIdentifier (Sort s) {
	static size_t i = 0;
	std::stringstream str;
	str << "##" << i;
	auto symb = std::make_shared<Symbol> (str.str());

	symb->setSort (s);
		
	auto ides = std::make_shared<Identifier> (*symb);
	vars.push_back(ides);
	identifiers.insert (std::make_pair (symb->getVal(),ides));
	return ides; 
  }
  
  std::unique_ptr<SMTLexer> lexer;
  LexObject object;
  LexObject objectBuf;
  
  ErrorHandler* handler = nullptr;
  std::unordered_map<std::string,std::shared_ptr<Identifier>> identifiers;
  std::vector<std::shared_ptr<Identifier> > vars;
  std::vector<ASTNode_ptr> asserts;
  NNFConverter converter;
};

}
#endif
