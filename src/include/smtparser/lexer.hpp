#ifndef _SMTLEXER__
#define _SMTLEXER__
#include <istream>
#if !defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "tokens.hpp"
namespace SMTParser {
class SMTLexer : public yyFlexLexer {
  private:
	int mylineno = 1;
	int column = 1;
	
	ReservedChecker kw;
	
	Tokens tok;
	
	
	int getLineNumber () {
	  return mylineno;
	}
	
	int getColumn () {
	  return column;
	}

  int toInt (Tokens t) {tok = t; return static_cast<int> (t);}  
public:
  virtual int yylex() override;
  LexObject getLex () {return LexObject (YYText(),tok, mylineno,column);}
  

  };

std::unique_ptr<SMTLexer>  makeFlexer (std::istream& i);
}

#endif
